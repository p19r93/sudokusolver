cmake_minimum_required(VERSION 3.19)
project(SudokuSolverC C)

set(CMAKE_C_STANDARD 99)

add_executable(SudokuSolver main.c SudokuGrid.c SudokuGrid.h testSudokuGrid.c testSudokuGrid.h Solver.c
        Solver.h testSolver.c testSolver.h)

add_executable(TestSudokuSolver runTests.c SudokuGrid.c SudokuGrid.h testSudokuGrid.c testSudokuGrid.h Solver.c
        Solver.h testSolver.c testSolver.h)