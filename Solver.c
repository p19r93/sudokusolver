//
// Created by Peter on 24/06/2021.
//

#include "testSudokuGrid.h"
#include "SudokuGrid.h"
#include "Solver.h"

int hasSolution(sudokuGrid game) {
    int solved = FALSE;
    if (isFull(game)) { return TRUE; }
    cell firstEmptyCell = getFirstEmptyCell(game);
    value trialValue = MIN_VALUE;
    while ( !solved && trialValue <= MAX_VALUE) {
        if (isLegal(game, firstEmptyCell, trialValue)) {
            setCell(game, firstEmptyCell, trialValue);
            solved = hasSolution(game);
        }
        if (!solved){ clearCell(game, firstEmptyCell); }
        trialValue++;
    }
    return solved;
}