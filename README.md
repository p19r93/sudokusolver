# SudokuSolver
This is a practice project for me to remind myself of some C best practices. I was inspired by the excellent [lecture series](https://youtube.com/playlist?list=PL6B940F08B9773B9F) by Richard Buckland of UNSW, on YouTube, which discusses this problem with his students as an example.
