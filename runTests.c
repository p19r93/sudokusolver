//
// Created by Peter on 24/06/2021.
//

#include <stdio.h>

#include "testSudokuGrid.h"
#include "testSolver.h"

int main() {
    printf("Running full test suite...\n");
    testSudokuGrid();
    testSolver();
    printf("All tests passed!\n");
    return 0;
}


