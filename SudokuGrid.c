//
// Created by peter on 21/06/2021.
//

#include <stdio.h>

#include "SudokuGrid.h"


static int isLegalRow(sudokuGrid game, cell candidateCell, value trialValue);
static int isLegalCol(sudokuGrid game, cell candidateCell, value trialValue);
static int isLegalBox(sudokuGrid game, cell candidateCell, value trialValue);
static int getRowNum(cell thisCell);
static int getColNum(cell thisCell);
static int getBoxNum(cell thisCell);
static cell getRowStartCell(cell thisCell);
static cell getColStartCell(cell thisCell);
static cell getBoxStartCell(cell thisCell);


int isValidValue(value val) {
    if ((val <= MAX_VALUE && val >= MIN_VALUE) || val == EMPTY_VALUE){
        return TRUE;
    }
    return FALSE;
}

int readGame(sudokuGrid game) {
    printf("Please enter the game you wish to solve.");
    cell thisCell = 0;
    value buf[GRID_SIZE];
    value val;
    scanf("%s", buf);
    while (thisCell <= MAX_CELL){
        val = buf[thisCell];
        if (!isValidValue(val)){
            return FALSE;
        }
        game[thisCell] = val;
        thisCell++;
    }
    return TRUE;
}

int readGameFromBuffer(sudokuGrid game, char* buf) {
    cell thisCell = 0;
    value val;
    while (thisCell <= MAX_CELL){
        val = buf[thisCell];
        if (!isValidValue(val)){
            return FALSE;
        }
        game[thisCell] = val;
        thisCell++;
    }
    return TRUE;
}

void showGame(sudokuGrid game) {
    cell currentCell = 0;
    int row;
    int col;
    printf("-------------------------------\n");
    while (currentCell <= MAX_CELL){
        row = currentCell / NUM_VALUES;
        col = currentCell % NUM_VALUES;
        if (col == 0 && row != 0) {
            printf("|\n");
            if (row % BOX_SIZE == 0){
                printf("-------------------------------\n");
            }
            printf("|");
        } else if (col % BOX_SIZE == 0){
            printf("|");
        }
        printf(" %c ", game[currentCell]);
        currentCell++;
    }
    printf("|\n-------------------------------\n");
}

cell getFirstEmptyCell(sudokuGrid game) {
    cell candidateCell = 0;
    while (candidateCell <= MAX_CELL){
        if (game[candidateCell] == EMPTY_VALUE){
            return candidateCell;
        }
        candidateCell++;
    }
    return MAX_CELL + 1;  // Not sure this is the right decision, could even be dangerous if not caught elsewhere
}

int isLegal(sudokuGrid game, cell candidateCell, value trialValue) {
    if (
            isLegalRow(game, candidateCell, trialValue)
            && isLegalCol(game, candidateCell, trialValue)
            && isLegalBox(game, candidateCell, trialValue)
    ) {
        return TRUE;
    } else {
        return FALSE;
    }
}

int isLegalRow(sudokuGrid game, cell candidateCell, value trialValue) {
    cell rowStartCell = getRowStartCell(candidateCell);
    cell rowEndCell = rowStartCell + NUM_VALUES - 1;
    cell thisCell = rowStartCell;
    while(thisCell <= rowEndCell){
        if (game[thisCell] == trialValue){
            return FALSE;
        }
        thisCell++;
    }
    return TRUE;
}

int isLegalCol(sudokuGrid game, cell candidateCell, value trialValue) {
    cell colStartCell = getColStartCell(candidateCell);
    cell thisCell = colStartCell;
    while (thisCell <= MAX_CELL){
        if (game[thisCell] == trialValue){
            return FALSE;
        }
        thisCell += NUM_VALUES;
    }
    return TRUE;
}

int isLegalBox(sudokuGrid game, cell candidateCell, value trialValue) {
    cell boxStartCell = getBoxStartCell(candidateCell);
    cell thisCell;
    int boxRow;
    int boxCol;
    boxRow = 0;
    while (boxRow < BOX_SIZE){
        boxCol = 0;
        while (boxCol < BOX_SIZE){
            thisCell = boxStartCell + (boxRow * NUM_VALUES) + boxCol;
            if (game[thisCell] == trialValue){
                return FALSE;
            }
            boxCol++;
        }
        boxRow++;
    }
    return TRUE;
}

cell getRowStartCell(cell thisCell) {
    return getRowNum(thisCell) * NUM_VALUES;
}

static cell getColStartCell(cell thisCell){
    return getColNum(thisCell);
}

static cell getBoxStartCell(cell thisCell){
    int boxNum = getBoxNum(thisCell);
    return (NUM_VALUES * BOX_SIZE) * (boxNum / BOX_SIZE) + BOX_SIZE * (boxNum % BOX_SIZE);
}

static int getRowNum(cell thisCell) {
    return thisCell / NUM_VALUES;
}

static int getColNum(cell thisCell) {
    return thisCell % NUM_VALUES;
}

static int getBoxNum(cell thisCell) {
    return (getRowNum(thisCell) / BOX_SIZE) * BOX_SIZE + getColNum(thisCell) / BOX_SIZE;
}

int readGameFromFile(sudokuGrid game, FILE *file) {
    cell thisCell = 0;
    value val;
    while (thisCell <= MAX_CELL) {
        val = (value)fgetc(file);
        if (!isValidValue(val)){
            return FALSE;
        }
        game[thisCell] = val;
        thisCell++;
    }
    return TRUE;
}

int writeGameToFile(sudokuGrid game, FILE *file){
    cell thisCell = 0;
    while (thisCell <= MAX_CELL) {
        fputc(game[thisCell], file);
        thisCell++;
    }
    return TRUE;
}

int isFull(sudokuGrid game) {
    cell candidateCell = 0;
    while (candidateCell <= MAX_CELL){
        if (game[candidateCell] == EMPTY_VALUE){
            return FALSE;
        }
        candidateCell++;
    }
    return TRUE;
}

void setCell(sudokuGrid game, cell thisCell, value thisValue) {
    game[thisCell] = thisValue;
}

value getCell(sudokuGrid game, cell thisCell) {
    return game[thisCell];
}

void clearCell(sudokuGrid game, cell thisCell) {
    game[thisCell] = EMPTY_VALUE;
}

void clearGame(sudokuGrid game){
    cell currentCell = 0;
    while (currentCell <= MAX_CELL){
        game[currentCell] = EMPTY_VALUE;
        currentCell++;
    }
}