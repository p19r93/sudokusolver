//
// Created by peter on 21/06/2021.
//
#include <stdio.h>

#ifndef SUDOKUSOLVERC_SUDOKUGRID_H
#define SUDOKUSOLVERC_SUDOKUGRID_H

#endif //SUDOKUSOLVERC_SUDOKUGRID_H

#define MIN_VALUE '1'
#define MAX_VALUE '9'
#define EMPTY_VALUE '.'
#define NUM_VALUES (MAX_VALUE - MIN_VALUE + 1)
#define BOX_SIZE 3
#define GRID_SIZE (NUM_VALUES * NUM_VALUES)
#define MAX_CELL (GRID_SIZE - 1)

#define TRUE 1
#define FALSE 0


typedef int cell;
typedef char value;
typedef value sudokuGrid[GRID_SIZE];

// Read the game in from stdin
int readGame(sudokuGrid game);

// Read the game in from a buffer
int readGameFromBuffer(sudokuGrid game, char* buf);

// Read a single game from a file (first MAX_CELL values)
int readGameFromFile(sudokuGrid game, FILE* file);

// Write a game out to a file
int writeGameToFile(sudokuGrid game, FILE * file);

// Display the game in the output
void showGame(sudokuGrid game);

// Check whether a given value is legal
int isValidValue(value val);

// Get the first empty cell in the grid
cell getFirstEmptyCell(sudokuGrid game);

// Check whether a grid is legal by the rules of the game
int isLegal(sudokuGrid game, cell candidateCell, value trialValue);

// Check whether a grid is full
int isFull(sudokuGrid game);

// Insert a value into a cell in a game
void setCell(sudokuGrid game, cell thisCell, value thisValue);

// Read a cell value
value getCell(sudokuGrid game, cell thisCell);

// Clear a cell
void clearCell(sudokuGrid game, cell thisCell);

// Clear the game
void clearGame(sudokuGrid game);