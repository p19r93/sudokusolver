#include <stdio.h>
#include <stdlib.h>

#include "SudokuGrid.h"
#include "Solver.h"

static void runTerminalGame();
static void runGameFromFile(FILE *inputFile, FILE *outputFile);

int main(int argc, char **argv) {
    FILE *inFile, *outFile;
    switch (argc) {
        case 1:  // No parameters, get game from stdin
            runTerminalGame();
            break;
        case 2:  // Single parameter is filename of input
            inFile = fopen(argv[1], "r");
            outFile = fopen("output.txt", "w");
            if (inFile == NULL ) {
                puts("Can't open input file.\n");
                exit(0);
            }
            if (outFile == NULL) {
                puts("Can't open output file.\n" );
                exit(0);
            }
            runGameFromFile(inFile, outFile);
            fclose(inFile);
            fclose(outFile);
            break;
        default:
            puts("Invalid arguments provided.");
            exit(1);
    }
    return 0;
}

static void runTerminalGame() {
    sudokuGrid game;
    readGame(game);
    showGame(game);
    if (hasSolution(game)) {
        printf("\nSolved:\n");
        showGame(game);
    } else {
        printf("Game has no solution. Finished with:\n\n");
        showGame(game);
    }
}

static void runGameFromFile(FILE *inputFile, FILE *outputFile) {
    sudokuGrid game;
    unsigned int gameCounter = 0;
    char nextChar = '\n';
    char reachedEnd = FALSE;
    while (!reachedEnd) {  // Consume the \n or other separator between games, watch for EOF
        clearGame(game);
        if(!readGameFromFile(game, inputFile)){ printf("Failed to read game.\n"); exit(0); };
        if (hasSolution(game)) {
            printf("Solved game %d\n", gameCounter);
        } else {
            printf("Game %d has no solution\n", gameCounter);
        }
        writeGameToFile(game, outputFile);
        fputc('\n', outputFile);
        nextChar = (char)fgetc(inputFile);
        while ( !isValidValue(nextChar) ){
            nextChar = (char)fgetc(inputFile);
            if (nextChar == EOF) { reachedEnd = TRUE; break; }
        }
        ungetc(nextChar, inputFile);  // Put the valid character we just read back in the stream
        gameCounter++;
    }
}
