//
// Created by peter on 21/06/2021.
//

#include <stdio.h>
#include <assert.h>

#include "testSudokuGrid.h"
#include "SudokuGrid.h"

static void testReadGameFromFile();
static void testWriteGameToFile();
static void testClearGame();
static void testGetSetCell();
static void testIsFull();
static void testIsLegal();

void testSudokuGrid() {
    printf("Testing sudokuGrid.c...\n");
    testReadGameFromFile();
    testWriteGameToFile();
    testClearGame();
    testGetSetCell();
    testIsFull();
    testIsLegal();
    printf("All sudokuGrid tests passed!\n");
}

static void testClearGame(){
    printf("Testing clearGame...   ");
    sudokuGrid game = {
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
    };

    clearGame(game);

    cell currentCell = 0;
    while (currentCell <= MAX_CELL){
        assert(game[currentCell] == EMPTY_VALUE);
        currentCell++;
    }
    printf("clearGame test complete.\n");
}

static void testGetSetCell(){
    printf("Testing testGetSetCell...   ");
    sudokuGrid emptyGame = {
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
    };

    cell candidateCell = 0;
    while (candidateCell <= MAX_CELL){
        assert(getCell(emptyGame, candidateCell) == '.');
        candidateCell++;
    }

    candidateCell = 0;
    value trialValue = MIN_VALUE;
    while(candidateCell <= MAX_CELL){
        setCell(emptyGame, candidateCell, trialValue);
        assert(getCell(emptyGame, candidateCell) == trialValue);
        candidateCell++;
    }

    printf("getSetCell test complete.\n");
}

static void testIsFull(){
    printf("Testing isFull...   ");
    sudokuGrid fullGame = {
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
    };

    sudokuGrid notFullGame = {
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '1', '2', '3', '4', '.', '.', '.', '.', '.',
    };

    sudokuGrid emptyGame = {
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
    };

    assert(isFull(fullGame) == TRUE);
    assert(isFull(notFullGame) == FALSE);
    assert(isFull(emptyGame) == FALSE);

    printf("isFull test complete.\n");
}

static void testIsLegal(){
    printf("Testing isLegal...   ");
    // Check all values are illegal in an impossible grid
    sudokuGrid impossibleGrid = {
        '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '1', '2', '3', '4', '5', '6', '7', '8', '.',
    };

    value trialValue = MIN_VALUE;
    while (trialValue <= MAX_VALUE){
        assert (isLegal(impossibleGrid, MAX_CELL, trialValue) == FALSE);
        trialValue++;
    }

    // Check a legal value is accepted anywhere in an empty grid
    sudokuGrid emptyGame = {
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
    };

    trialValue = MIN_VALUE;
    cell candidateCell = 0;
    while (candidateCell <= MAX_CELL){
        while (trialValue <= MAX_VALUE){
            assert(isLegal(emptyGame, candidateCell, trialValue) == TRUE);
            trialValue++;
        }
        trialValue = MIN_VALUE;
        candidateCell++;
    }

    // Check illegal row only is caught
    sudokuGrid rowTestGame = {
            '1', '2', '3', '4', '5', '6', '7', '8', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
    };
    assert(isLegal(rowTestGame, 8, '9') == TRUE);
    assert(isLegal(rowTestGame, 8, '1') == FALSE);

    // Check illegal column only is caught
    sudokuGrid colTestGame = {
            '1', '.', '.', '.', '.', '.', '.', '.', '.',
            '2', '.', '.', '.', '.', '.', '.', '.', '.',
            '3', '.', '.', '.', '.', '.', '.', '.', '.',
            '4', '.', '.', '.', '.', '.', '.', '.', '.',
            '5', '.', '.', '.', '.', '.', '.', '.', '.',
            '6', '.', '.', '.', '.', '.', '.', '.', '.',
            '7', '.', '.', '.', '.', '.', '.', '.', '.',
            '8', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
    };
    candidateCell = NUM_VALUES * (NUM_VALUES - 1);
    assert(isLegal(colTestGame, candidateCell, '9') == TRUE);
    assert(isLegal(colTestGame, candidateCell, '1') == FALSE);

    // Check illegal box only is caught
    sudokuGrid boxTestGame = {
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '1', '2', '3', '.', '.', '.',
            '.', '.', '.', '4', '5', '6', '.', '.', '.',
            '.', '.', '.', '7', '8', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
            '.', '.', '.', '.', '.', '.', '.', '.', '.',
    };
    candidateCell = NUM_VALUES * 5 + 5;
    assert(isLegal(boxTestGame, candidateCell, '9') == TRUE);
    assert(isLegal(boxTestGame, candidateCell, '1') == FALSE);

    printf("isLegal test complete.\n");
}

static void testReadGameFromFile() {
    printf("Testing readGameFromFile...   ");
    sudokuGrid testFileFirstGame = {
            '.', '.', '3', '.', '2', '.', '6', '.', '.',
            '9', '.', '.', '3', '.', '5', '.', '.', '1',
            '.', '.', '1', '8', '.', '6', '4', '.', '.',
            '.', '.', '8', '1', '.', '2', '9', '.', '.',
            '7', '.', '.', '.', '.', '.', '.', '.', '8',
            '.', '.', '6', '7', '.', '8', '2', '.', '.',
            '.', '.', '2', '6', '.', '9', '5', '.', '.',
            '8', '.', '.', '2', '.', '3', '.', '.', '9',
            '.', '.', '5', '.', '1', '.', '3', '.', '.',
    };

    FILE* testFile;
    testFile = fopen("easy50.txt", "r");
    assert(testFile != NULL);
    sudokuGrid game;
    readGameFromFile(game, testFile);
    fclose(testFile);

    cell currentCell = 0;
    while(currentCell <= MAX_CELL) {
        assert (game[currentCell] == testFileFirstGame[currentCell]);
        currentCell++;
    }
    printf("readGameFromFile test complete.\n");
}

void testWriteGameToFile() {
    printf("Testing writeGameToFile...   ");
    sudokuGrid testGame = {
            '.', '.', '3', '.', '2', '.', '6', '.', '.',
            '9', '.', '.', '3', '.', '5', '.', '.', '1',
            '.', '.', '1', '8', '.', '6', '4', '.', '.',
            '.', '.', '8', '1', '.', '2', '9', '.', '.',
            '7', '.', '.', '.', '.', '.', '.', '.', '8',
            '.', '.', '6', '7', '.', '8', '2', '.', '.',
            '.', '.', '2', '6', '.', '9', '5', '.', '.',
            '8', '.', '.', '2', '.', '3', '.', '.', '9',
            '.', '.', '5', '.', '1', '.', '3', '.', '.',
    };

    FILE* outputFile;
    outputFile = fopen("test/testOutput.txt", "w");
    assert(outputFile != NULL);
    writeGameToFile(testGame, outputFile);

    sudokuGrid readGame;
    readGameFromFile(readGame, outputFile);
    cell currentCell = 0;
    while (currentCell <= MAX_CELL) {
        assert (readGame[currentCell] == testGame[currentCell]);
        currentCell++;
    }
    fclose(outputFile);
    printf("writeGameToFile test complete.\n");
}
